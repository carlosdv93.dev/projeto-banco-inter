package com.carlosdv93.demo.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Usuario implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	
	private String nome;
	private String email;
	
	@OneToMany(targetEntity=DigitoUnico.class,cascade = CascadeType.ALL , fetch = FetchType.LAZY, mappedBy = "usuario")
	private List<DigitoUnico> digitos;

	public Long getId() {
		return id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public List<DigitoUnico> getDigitos() {
		return digitos;
	}

	public void setDigitos(List<DigitoUnico> digitos) {
		this.digitos = digitos;
	}
}

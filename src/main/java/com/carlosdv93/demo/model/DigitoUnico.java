package com.carlosdv93.demo.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.carlosdv93.demo.dto.DigitosResultadoDTO;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class DigitoUnico implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idDigito;
	private int vezes;
	private String numero;
	private int resultado;
	
	@JsonIgnore
	@ManyToOne(targetEntity=Usuario.class, fetch = FetchType.LAZY)
	//@JoinColumn(name="companyId")//Optional
	private Usuario usuario;

	public Long getidDigito() {
		return idDigito;
	}

	public int getVezes() {
		return vezes;
	}

	public void setVezes(int vezes) {
		this.vezes = vezes;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public int getResultado() {
		return resultado;
	}

	public void setResultado(int resultado) {
		this.resultado = resultado;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public static DigitoUnico fromDTO(DigitosResultadoDTO dto) {
		DigitoUnico digito = new DigitoUnico();
		digito.setNumero(dto.getNumero());
		digito.setResultado(dto.getResultado());
		digito.setVezes(dto.getVezes());
		return digito;
	}
	
	public static DigitoUnico fromDTOAndUser(DigitosResultadoDTO dto, Usuario user) {
		DigitoUnico digito = new DigitoUnico();
		digito.setNumero(dto.getNumero());
		digito.setResultado(dto.getResultado());
		digito.setVezes(dto.getVezes());
		digito.setUsuario(user);
		return digito;
	}


}

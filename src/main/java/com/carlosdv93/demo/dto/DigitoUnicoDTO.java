package com.carlosdv93.demo.dto;

public class DigitoUnicoDTO {
	
	private String numero;
	private int vezes;
	
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public int getVezes() {
		return vezes;
	}
	public void setVezes(int vezes) {
		this.vezes = vezes;
	}
}

package com.carlosdv93.demo.dto;

public class DigitosResultadoDTO {
	
	private int vezes;
	private String numero;
	private int resultado;

	public int getVezes() {
		return vezes;
	}

	public void setVezes(int vezes) {
		this.vezes = vezes;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public int getResultado() {
		return resultado;
	}

	public void setResultado(int resultado) {
		this.resultado = resultado;
	}
	
	@Override
	public String toString() {
		return "DigitoResultado" + 
				"Numero: " + getNumero() +
				" Vezes: " + getVezes() +
				"Result: " + getResultado() + "\n";
	}

}

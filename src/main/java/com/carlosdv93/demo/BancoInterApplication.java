package com.carlosdv93.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BancoInterApplication {

	public static void main(String[] args) {
		SpringApplication.run(BancoInterApplication.class, args);
	}

}

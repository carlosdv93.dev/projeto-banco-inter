package com.carlosdv93.demo.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.carlosdv93.demo.model.Usuario;

@Service
public interface UsuarioService {

	public List<Usuario> findByEmail(String email);

	public Usuario findById(Long id);

	public Usuario insertUsuario(Usuario usuario);

	public void deleteUsuarioById(Long id);

	public Usuario updateUsuario(Usuario usuario);

}

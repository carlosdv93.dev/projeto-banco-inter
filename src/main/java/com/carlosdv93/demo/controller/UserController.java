package com.carlosdv93.demo.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.carlosdv93.demo.dto.UserDto;
import com.carlosdv93.demo.model.Usuario;
import com.carlosdv93.demo.repository.DigitoUnicoRepository;
import com.carlosdv93.demo.repository.UsuarioRepository;

@RequestMapping("/api/user")
@RestController
public class UserController {

	@Autowired
	private UsuarioRepository usuarioRepo;
	
	@GetMapping
	public Iterable<Usuario> buscarUsuarios() {
		return usuarioRepo.findAll();
	}

	@PostMapping
	public @ResponseBody Usuario criarUsuario(@RequestBody UserDto user) {
		Usuario usuario = new Usuario();
		usuario.setNome(user.getNome());
		usuario.setEmail(user.getEmail());

		return usuarioRepo.save(usuario);
	}

	@GetMapping(path = "/{id}")
	public ResponseEntity<Usuario> buscarUsuario(@PathVariable("id") Long id) {
		Optional<Usuario> user = usuarioRepo.findById(id);

		if (user.isPresent()) {
			return ResponseEntity.ok().body(user.get());
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@PutMapping(path = "/{id}")
	public ResponseEntity<Usuario> atualizaUsuario(@PathVariable("id") Long id, @RequestBody UserDto usuarioAlterado) {
		Optional<Usuario> user = usuarioRepo.findById(id);

		if (user.isPresent()) {
			user.get().setNome(usuarioAlterado.getNome());
			user.get().setEmail(usuarioAlterado.getEmail());
			return ResponseEntity.ok().body(usuarioRepo.save(user.get()));
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@DeleteMapping(path = "/{id}")
	public ResponseEntity<?> deletarUsuario(@PathVariable("id") Long id) {
		Optional<Usuario> user = usuarioRepo.findById(id);
		
		if(user.isPresent()) {
			usuarioRepo.delete(user.get());
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@GetMapping(path = "/{id}/digitos")
	public ResponseEntity<Usuario> buscarDigitosPorUsuario(@PathVariable("id") Long id) {
		Optional<Usuario> user = usuarioRepo.findById(id);

		if (user.isPresent()) {
			return ResponseEntity.ok().body(user.get());
		} else {
			return ResponseEntity.notFound().build();
		}
	} 
}

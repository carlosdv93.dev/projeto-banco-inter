package com.carlosdv93.demo.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.carlosdv93.demo.cache.CacheDigitos;
import com.carlosdv93.demo.dto.DigitoUnicoDTO;
import com.carlosdv93.demo.dto.DigitosResultadoDTO;
import com.carlosdv93.demo.model.DigitoUnico;
import com.carlosdv93.demo.model.Usuario;
import com.carlosdv93.demo.repository.DigitoUnicoRepository;
import com.carlosdv93.demo.repository.UsuarioRepository;

@RequestMapping("/api/digito-unico")
@RestController
public class DigitoUnicoController {
	
	@Autowired
	private DigitoUnicoRepository digitoUnicoRepo;
	
	@Autowired
	private UsuarioRepository usuarioRepo;
	
	@GetMapping(path = "/{idUser}")
	public ResponseEntity<?> calcularDigitoUnico(@PathVariable("idUser") Long idUser, @RequestBody DigitoUnicoDTO digitoParam) {
		
		String numero = digitoParam.getNumero();
		int vezes = digitoParam.getVezes();
		
		//valida se o usuario existe
		Optional<Usuario> usuario = usuarioRepo.findById(idUser);
		
		if(!usuario.isPresent()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Usuário não encontrado");
		}

		//Valida Cache
		DigitosResultadoDTO digito = CacheDigitos.validaExistenciaCache(numero, vezes);

		//Cria DigitoUnico para inserir
		DigitosResultadoDTO digitoCalculado = new DigitosResultadoDTO();
		
		
		if(digito != null) {
			System.out.println("Digito CACHE: " + digito.toString());
			digitoUnicoRepo.save(DigitoUnico.fromDTOAndUser(digito, usuario.get()));
			return ResponseEntity.ok(digito.getResultado()); 
		}
		

		digitoCalculado.setNumero(numero);
		digitoCalculado.setVezes(vezes);
		
		Long aux = 0L;
		
		if(numero.length() <= 1) {
			digitoCalculado.setResultado(aux.intValue());
			CacheDigitos.setDigitosResultados(digitoCalculado);
			digitoUnicoRepo.save(DigitoUnico.fromDTOAndUser(digitoCalculado, usuario.get()));
			return ResponseEntity.ok(aux.intValue());
		}

		String numString = String.valueOf(numero);
		StringBuilder numAuxString = new StringBuilder(numString);
		for(int i = 1; i <= vezes-1; i++) {
			numAuxString.append(numString);
		}
		
		System.out.println("Numero: " + numAuxString);
		
		while(numAuxString.length() > 1) {
			int digitoUnico = 0;
			for (int i = 0; i < numAuxString.chars().count(); i++) {
				digitoUnico += Integer.parseInt(String.valueOf(numAuxString.charAt(i)));
				System.out.println(digitoUnico);
			}
			System.out.println("Soma: " + digitoUnico);
			numAuxString.replace(0, numAuxString.length(), String.valueOf(digitoUnico));
			aux = Long.valueOf(digitoUnico);
		}
		
		digitoCalculado.setResultado(aux.intValue());
		
		CacheDigitos.setDigitosResultados(digitoCalculado);
		//salva no banco de dados
		digitoUnicoRepo.save(DigitoUnico.fromDTOAndUser(digitoCalculado, usuario.get()));
	
		return ResponseEntity.ok(aux.intValue()); 
	}

}

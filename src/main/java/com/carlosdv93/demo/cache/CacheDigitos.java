package com.carlosdv93.demo.cache;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Stream;

import com.carlosdv93.demo.dto.DigitosResultadoDTO;

public final class CacheDigitos {
	
	private CacheDigitos() {
	} // só pra garantir que não haja instâncias dela

	private static DigitosResultadoDTO[] digitosResultados; //= new DigitosResultadoDTO[10];

	public static DigitosResultadoDTO[] getDigitosResultados() {
		System.out.println("CACHE: " + Arrays.toString(digitosResultados));
		return digitosResultados;
	}

	public static void setDigitosResultadosArray(DigitosResultadoDTO[] digitosResultados) {
		CacheDigitos.digitosResultados = digitosResultados;
	}
	
	public static void setDigitosResultados(DigitosResultadoDTO digito) {
		
		if(digitosResultados == null) {
			digitosResultados = new DigitosResultadoDTO[1];
			digitosResultados[0] = digito;
		} else {
			System.out.println("CACHE: " + Arrays.toString(digitosResultados));
			DigitosResultadoDTO[] arrayAux = digitosResultados.clone();
			
			int tamanho = getDigitosResultados().length;
			if(tamanho < 10) {
				digitosResultados = new DigitosResultadoDTO[tamanho+1];
				for(int i=tamanho; i > 0; i--) {
					digitosResultados[i] = arrayAux[i-1];
					System.out.println(i);
				}
				
				digitosResultados[0] = digito;
			} else {
				for(int i=tamanho-1; i > 0; i--) {
					digitosResultados[i] = arrayAux[i-1];
				}
				
				digitosResultados[0] = digito;
			}
		}

	}
	
	public static DigitosResultadoDTO validaExistenciaCache(String numero, int vezes) {
		
		if(digitosResultados == null) {
			return null;
		}
		
		System.out.println("CACHE: " + Arrays.toString(digitosResultados));
		
		Stream<DigitosResultadoDTO> stream = Stream.of(digitosResultados);
		
		Optional<DigitosResultadoDTO> digitoUnico = stream.filter(digito -> digito.getNumero().equals(numero) && digito.getVezes() == vezes).findFirst();
		
		return digitoUnico.orElse(null);
			
	}
}

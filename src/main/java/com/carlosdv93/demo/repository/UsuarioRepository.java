package com.carlosdv93.demo.repository;

import org.springframework.data.repository.CrudRepository;

import com.carlosdv93.demo.model.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Long>{

}

package com.carlosdv93.demo.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.carlosdv93.demo.model.DigitoUnico;
import com.carlosdv93.demo.model.Usuario;

public interface DigitoUnicoRepository extends CrudRepository<DigitoUnico, Long>{
	public List<Usuario> findByUsuario(Usuario usuario);
}
